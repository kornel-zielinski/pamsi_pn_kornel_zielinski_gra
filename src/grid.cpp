#include "grid.hh"

using namespace tic;

grid::grid()
{
  _grid = new int[_size*_size];
  for(int i=0; i<_size*_size; i++)
    _grid[i]=0;      
}

grid::grid(int size, int length)
{
  _len  = length;
  _size = size;
  _grid = new int[_size*_size];
  for(int i=0; i<_size; i++)
    _grid[i]=0;
}

bool grid::move(int what, int where)
{
  if(_grid[where]==0 && (what == -1 || what == 1))
    _grid[where]=what;
  else
    return false;
  return true;
}

void grid::print()
{
  for(int i=0; i<_size*_size; i++)
  {
    switch(_grid[i])
      {
      case 1:
        std::cout << 'O';
        break;
      case 0:
        std::cout << '_';
        break;
      case -1:
        std::cout << 'X';
        break;
      default:
        std::cout << "what.";
        break;
      }
    if((i+1)%_size == 0)
      std::cout << '\n';
    else
      std::cout << ' ';
  }
      
}

bool grid::move(char what, int column, int row)
{
  if(row>=_size || column >=_size)
    return false;
  int where = row*_size+column;
  int what_int;
  if(what == 'X' || what == 'x')
    what_int = -1;
  else if(what == 'O' || what == 'o')
    what_int = 1;
  else
    return false;
  return move(what_int, where);
}

int grid::check_line(int* tab, int size, int len)
{
  int k = len;

  if(len>size)
    return 0;
  for(int i=0; i<size-1; i++)
  {
    if(tab[i] == tab[i+1] && tab[i] != 0)
    {
      k--;
      if(k == 1)
	return tab[i];
    }
    else
      k = len;
  }
  return 0;
}

int grid::check_win()
{
  int win = 0;
  int* tab = new int[_size];
  bool full=true;
  
  for(int i=0; i<_size; i++)
  {  //poziom
    for(int j=0; j<_size; j++)
      tab[j]=_grid[i*_size+j];
    win = check_line(tab, _size, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
    }
      //pion
    for(int j=0; j<_size; j++)
      tab[j]=_grid[j*_size+i];
    win = check_line(tab, _size, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
    }

    //skos prawo-dol
    for(int j=0; j<_size-i; j++)
      tab[j]=_grid[j*_size+j+i];
    win = check_line(tab, _size-i, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
    }

    for(int j=0; j<_size-i; j++)
      tab[j]=_grid[j*_size+j+i*_size];
    win = check_line(tab, _size-i, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
    }
   
        //skos lewo-dol
    for(int j=0; j<_size-i; j++)
      tab[j]=_grid[j*_size+(_size-j-1)-i];
    win = check_line(tab, _size, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
    }
  
    for(int j=0; j<_size-i; j++)
      tab[j]=_grid[j*_size+(_size-j-1)+i*_size];
    win = check_line(tab, _size-i, _len);
    if(win!=0)
    {
      delete[] tab;
      return win;
      }
  }
  
  delete[] tab;
  for(int i=0; i<_size*_size; i++)
  {
    if(_grid[i]==0)
      full = false;
  }
  if(full)
    return 2;
  else
    return 0;
}

void grid::copy(grid& original)
{
  if(_size != original.size())
    return;
  
  for(int i=0; i<_size; i++)
  {
    for(int j=0; j<_size; j++)
      _grid[_size*i+j]=original.peek(j, i);
  }
}

void grid::clear()
{
  for(int i=0; i<_size*_size; i++)
    _grid[i] = 0;
}
