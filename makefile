CXXFLAGS=-g -Iinc -pedantic -std=c++11
TicTacToe: obj obj/main.o obj/grid.o
	g++ ${CXXFLAGS} -o TicTacToe obj/main.o obj/grid.o
obj:
	mkdir obj
obj/main.o: obj src/main.cpp
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp
obj/grid.o: obj src/grid.cpp inc/grid.hh
	g++ -c ${CXXFLAGS} -o obj/grid.o src/grid.cpp
